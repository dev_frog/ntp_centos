#!/usr/bin/bash

echo "Updating OS"
sudo yum update -y
sudo yum upgrade -y


echo "Installing Some software"
sudo yum install -y vim tmux net-tools httpd samba ftp 

# this section for ntp set
echo "Setup ip Config"

sudo yum install -y chrony


sed -i "s/server 0.asia.pool.ntp.org iburst/server 0.asia.pool.ntp.org iburst/g" /etc/chrony.conf
sed -i "s/server 1.asia.pool.ntp.org iburst/server 1.asia.pool.ntp.org iburst/g" /etc/chrony.conf

sed -i "s/allow 192.168.0.0\/24/allow 192.168.0.1\/24/g" /etc/chrony.conf


sudo systemctl start chronyd
sudo systemctl enable chronyd
firewall-cmd --add-service=ntp --permanent
firewall-cmd --reload

sudo chronyc sources

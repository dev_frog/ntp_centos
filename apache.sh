#!/usr/bin/bash

echo "--Apache configuration--"

sudo yum update -y
sudo yum upgrade -y

sudo yum install httpd -y

sudo systemctl start httpd
sudo systemctl enable httpd

sudo firewall-cmd --add-service=http --permanent 
sudo firewall-cmd --reload



# config

mkdir -p /var/www/$1/html
mkdir -p /var/www/$1/log

echo "IncludeOptional site_enable/*.conf" >> /etc/httpd/conf/httpd.conf

mkdir -p /etc/httpd/sites-available /etc/httpd/site_enable

echo "<VirtualHost *:80>
    ServerName www.$1
    ServerAlias $1
    DocumentRoot /var/www/$1/html
    ErrorLog /var/www/$1/log/error.log
    CustomLog /var/www/$1/log/requests.log combined
    </VirtualHost>" > /etc/httpd/sites-available/$1.conf


sudo ln -s /etc/httpd/sites-available/$1.conf /etc/httpd/site_enable/$1.conf

sudo setsebool -P httpd_unified 1



sudo systemctl restart httpd
